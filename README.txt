Revision Authorship
===================

When a user updates a node, without creating a new revision, the original
revision author is overriden by the current user.

This module allows to preserve the revision authorship of a given content type:

1. Go to the node type form at /admin/structure/types/manage/YOUR_CONTENT_TYPE

2. Select 'Preserve revision authorship' on Publishing options.
